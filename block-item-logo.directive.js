(function () {

    'use strict';

    function blockItemLogo (PATH_CONFIG) {

        return {
            restrict: "E",
            transclude: false,
            replace: true,
            link: function (scope, element, attrs) {},
            controller: function ($scope, $controller) {

                angular.extend(this, $controller('baseController', {
                    $scope: $scope
                }));

            },

            templateUrl: PATH_CONFIG.BRAVOURE_COMPONENTS + 'angular-block-item-logo/block-item-logo.html'
        }
    }

    blockItemLogo.$inject = ['PATH_CONFIG'];

    angular
        .module('bravoureAngularApp')
        .directive('blockItemLogo', blockItemLogo);

})();
